#!/usr/bin/env/ python
from flask import Flask, request, send_file
from flask_cors import CORS, cross_origin
from io import StringIO as stringio
import threading
import time
import sys
sys.path.insert(0, '../../script_mapper/')
import middlewareAPI


########
#SERVERS
########

AD_App = Flask(__name__)
CORS(AD_App)

#########
#MAP URLS
#########

@AD_App.route("/", methods=["GET"])
def root():
	return "Startpage"

@AD_App.route("/instances", methods=["GET"])
def getInstances():
	return "All instances"

@AD_App.route("/instances/running", methods=["GET"])
def getRunningInstances():
	return "All running instances"

@AD_App.route("/instances/deleteAll", methods=["Post"])
def deleteAllInstances():
	return "Deleting all instances"

@AD_App.route("/instance/<username>/start", methods=["POST"])
def startSpecificInstance(username):
	filename = username +".rdp"
	f = stringio()
	middlewareAPI.getRdpFromName(username, f)
	return send_file(f, attachment_filename=filename,as_attachment=True)
	f.close()	

@AD_App.route("/instance/<username>/stop", methods=["POST"])
def stopSpecificInstance(username):
	middlewareAPI.stopInstance(username)
	return "Stopping instance of {0}".format(username)

@AD_App.route("/instance/<username>/delete", methods=["POST"])
def deleteSpecificInstance(username):
	middlewareAPI.deleteInstance(username)
	return "Deleting instance of {0}".format(username)

@AD_App.route("/instance/<username>", methods=["GET"])
def getSpecificInstance(username):
	return "Instance of {0}".format(username)


AD_App.run(host="0.0.0.0" ,port=9000, threaded=True)

