#!/usr/bin/env/ python
from flask import Flask, request, send_from_directory
from flask_cors import CORS, cross_origin
import threading
import time
import sys
sys.path.insert(0, '../script_mapper/')
import middlewareAPI


########
#SERVERS
########

AD_App = Flask(__name__)
ST_App = Flask(__name__)

CORS(AD_App)
CORS(ST_App)


#########
#MAP URLS
#########

#General
@ST_App.route("/", methods=["GET"])
@AD_App.route("/", methods=["GET"])
def root():
	return "Startpage"


#Admin
@AD_App.route("/instances", methods=["GET"])
def getInstances():
	return "All instances"

@AD_App.route("/instances/running", methods=["GET"])
def getRunningInstances():
	return "All running instances"

@AD_App.route("/instances/deleteAll", methods=["Post"])
def deleteAllInstances():
	return "Deleting all instances"

@AD_App.route("/instance/<username>/start", methods=["POST"])
def startSpecificInstance(username):
	filename = username +".rdp"
	f = open(filename,"w")
	middlewareAPI.getRdpFromName(username, f)
	f.close()
	return "Starting instance of {0}".format(username)

@AD_App.route("/instance/<username>/stop", methods=["POST"])
def stopSpecificInstance(username):
	middlewareAPI.stopInstance(username)
	return "Stopping instance of {0}".format(username)

@AD_App.route("/instance/<username>/delete", methods=["POST"])
def deleteSpecificInstance(username):
	middlewareAPI.deleteInstance(username)
	return "Deleting instance of {0}".format(username)

@AD_App.route("/instance/<username>", methods=["GET"])
def getSpecificInstance(username):
	return "Instance of {0}".format(username)


#Student
@ST_App.route("/myinstance", methods=["GET"])
def getStudentInstance():
	username = request.args.get("username")
	return "Your instance {0}".format(username)

@ST_App.route("/myinstance/start", methods=["POST"])
def startStudentInstance():
	username = request.form.get("username")
	filename = username +".rdp"
	f = open(filename,"w")
	middlewareAPI.getRdpFromName(username, f)
	f.close()
	return "Starting your instance {0}".format(username)

@ST_App.route("/myinstance/download", methods=["GET"])
def downloadrdp():
	username = request.args.get("username")
	filename = username +".rdp"
	return send_from_directory(".",
                               filename, as_attachment=True)

@ST_App.route("/myinstance/stop", methods=["POST"])
def stopSTudentInstance():
	username = request.form.get("username")
	middlewareAPI.stopInstance(username)
	return "Stopping your instance {0}".format(username)

@ST_App.route("/myinstance/delete", methods=["POST"])
def deleteStudentInstance():
	username = request.form.get("username")
	middlewareAPI.deleteInstance(username)
	return "Deleting your instance {0}".format(username)





#MISC
#####

#Wrappers
def ADrunWrapper():
	AD_App.run(port=9000)

def STrunWrapper():
	ST_App.run(port=9001)


#Calls
t = threading.Thread(target = ADrunWrapper)
t2 = threading.Thread(target = STrunWrapper)
t.daemon = True
t2.daemon = True
t.start()
t2.start()
while True:
	time.sleep(1)
