# README #
### What is this repository for? ###

A REST Server for our CloudLab.
Webfrontend calls a URL, REST Server associates a function to that URL.

### How do I get set up? ###

* `apt-get install python3-pip`
* `pip3 install Flask`
* `pip3 install flask-cors`
* `OBSOLETE (pip install web.py --user)`

### URLs

####Admin
#####GET
"/" : The root of the structure. For now returns "Startpage".

"/instances" : Note the plural. Returns all instances.

"/instances/running" : Again plural. Returns all running instances.

"/instance/10" : Singular here. 10 is an example. Returns info about instance 10.

#####POST
"/instance/10/start" : Starts instance number 10.

"/instance/10/stop" : Stops instance number 10.


####Students
#####GET
"/" : The root of the structure. For now returns "Startpage".

"/myinstance" : Returns information about the students instance.

#####POST
"/myinstance/start" : Starts the students instance.

for debug: "/myinstance/stop" : Stops the students instance.

"/myinstance/download?username=example" : downloads the rdp file for user "example"

for debug: "/myinstance/delete" : deletes the instance

###### (Obsolete) Setting the Port

THIS IS NOW DONE BY THE PROGRAM ITSELF AND IS NO LONGER NEEDED.

The port can be set using a commandline parameter

`python rest.py 9000` starts the server on port 9000.
Default port is 8080

### Used References

* http://www.dreamsyssoft.com/python-scripting-tutorial/create-simple-rest-web-service-with-python.php
* http://webpy.org/docs/0.3/
* http://stackoverflow.com/questions/14444913/web-py-specify-address-and-port
* https://pymotw.com/2/threading/
* http://stackoverflow.com/questions/11815947/cannot-kill-python-script-with-ctrl-c

