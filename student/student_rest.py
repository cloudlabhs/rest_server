#!/usr/bin/env/ python
from flask import Flask, request, send_file, abort
from flask_cors import CORS, cross_origin
from io import BytesIO as bytesio
import threading
import time
import sys
sys.path.insert(0, '../../script_mapper/')
import middlewareAPI


########
#SERVERS
########

ST_App = Flask(__name__)
CORS(ST_App)

#########
#MAP URLS
#########


@ST_App.route("/", methods=["GET"])
def root():
	return "Startpage"

@ST_App.route("/myinstance", methods=["GET"])
def getStudentInstance():
	username = request.args.get("username")
	return "Your instance {0}".format(username)

@ST_App.route("/myinstance/start", methods=["POST"])
def startStudentInstance():
        username = request.form.get("username")
        middlewareAPI.startInstance(username)
        return "Starting your instance"

@ST_App.route("/myinstance/download", methods=["GET"])
def downloadRdpFile():
        username = request.args.get("username")
        filename = username +".rdp"
        f = bytesio()
        if( middlewareAPI.getRdpFromName(username, f, "utf-8") == False):
            abort(404)
        else:
            f.seek(0)
            return send_file(f, attachment_filename=filename, as_attachment=True)
        f.close()

@ST_App.route("/myinstance/stop", methods=["POST"])
def stopSTudentInstance():
	username = request.form.get("username")
	middlewareAPI.stopInstance(username)
	return "Stopping your instance {0}".format(username)

@ST_App.route("/myinstance/delete", methods=["POST"])
def deleteStudentInstance():
	username = request.form.get("username")
	middlewareAPI.deleteInstance(username)
	return "Deleting your instance {0}".format(username)



ST_App.run(host="0.0.0.0", port=9001, threaded=True)
